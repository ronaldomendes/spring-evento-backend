package com.cursospring.springeventobackend.repository;

import com.cursospring.springeventobackend.models.Convidado;
import com.cursospring.springeventobackend.models.Evento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConvidadoRepository extends JpaRepository<Convidado, Long> {
    List<Convidado> findByEvento(Evento evento);
    Convidado findByRg(String rg);
}
