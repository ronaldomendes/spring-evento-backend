package com.cursospring.springeventobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEventoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringEventoBackendApplication.class, args);
	}

}
