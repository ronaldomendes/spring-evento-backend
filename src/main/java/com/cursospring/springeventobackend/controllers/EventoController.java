package com.cursospring.springeventobackend.controllers;

import com.cursospring.springeventobackend.models.Convidado;
import com.cursospring.springeventobackend.models.Evento;
import com.cursospring.springeventobackend.repository.ConvidadoRepository;
import com.cursospring.springeventobackend.repository.EventoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class EventoController {

    @Autowired
    private EventoRepository eventoRepository;

    @Autowired
    private ConvidadoRepository convidadoRepository;

    @RequestMapping(value = "/cadastrarEvento", method = RequestMethod.GET)
    public String form() {
        return "evento/formEvento";
    }

    @RequestMapping(value = "/cadastrarEvento", method = RequestMethod.POST)
    public String form(@Valid Evento evento, BindingResult result, RedirectAttributes attributes) {

        if (result.hasErrors()) {
            attributes.addFlashAttribute("Mensagem", "Verifique todos os campos!");
            return "redirect:/cadastrarEvento";
        }

        eventoRepository.save(evento);
        attributes.addFlashAttribute("Mensagem", "Evento cadastrado com sucesso!");
        return "redirect:/cadastrarEvento";
    }

    @RequestMapping(value = "/eventos")
    public ModelAndView listaEventos() {
        ModelAndView view = new ModelAndView("index");
        Iterable<Evento> eventos = eventoRepository.findAll();
        view.addObject("eventos", eventos);
        return view;
    }

    @RequestMapping(value = "/{codigo}", method = RequestMethod.GET)
    public ModelAndView detalhesEvento(@PathVariable("codigo") Long codigo) {
        Evento evento = eventoRepository.findByCodigo(codigo);
        ModelAndView view = new ModelAndView("evento/detalhesEvento");
        view.addObject("evento", evento);

        Iterable<Convidado> convidados = convidadoRepository.findByEvento(evento);
        view.addObject("convidados", convidados);
        return view;
    }

    @RequestMapping(value = "/deletarEvento")
    public String deletarEvento(Long codigo) {
        Evento evento = eventoRepository.findByCodigo(codigo);
        eventoRepository.delete(evento);
        return "redirect:/eventos";
    }

    @RequestMapping(value = "/{codigo}", method = RequestMethod.POST)
    public String detalhesEventoPost(@PathVariable("codigo") Long codigo, @Valid Convidado convidado,
                                     BindingResult result, RedirectAttributes attributes) {

        if (result.hasErrors()) {
            attributes.addFlashAttribute("Mensagem", "Verifique todos os campos!");
            return "redirect:/{codigo}";
        }

        Evento evento = eventoRepository.findByCodigo(codigo);
        convidado.setEvento(evento);
        convidadoRepository.save(convidado);
        attributes.addFlashAttribute("Mensagem", "Convidado adicionado com sucesso!");
        return "redirect:/{codigo}";
    }

    @RequestMapping(value = "/deletarConvidado")
    public String deletarConvidado(String rg) {
        Convidado convidado = convidadoRepository.findByRg(rg);
        convidadoRepository.delete(convidado);

        Evento evento = convidado.getEvento();
        String codigo = "" + evento.getCodigo();

        return "redirect:/" + codigo;
    }
}
